﻿using UnityEngine;
using System.Collections;

public class Background : MonoBehaviour {

	public Camera mainCamera;
	public float percentStickToCamera;
	Vector3 beginPoint;
	public bool riseWithSun;
	public Vector2 beginningOffset;

	// Use this for initialization
	void Start () 
	{
		transform.position = new Vector3(mainCamera.transform.position.x, mainCamera.transform.position.y, 0) + new Vector3(beginningOffset.x,beginningOffset.y, transform.position.z);
		beginPoint = transform.position;
	}
	
	// Update is called once per frame
	void LateUpdate () 
	{
		transform.position = (new Vector3(mainCamera.transform.position.x, mainCamera.transform.position.y, 0) - new Vector3(beginPoint.x,beginPoint.y, 0))*percentStickToCamera 
			+ beginPoint + new Vector3(beginningOffset.x, beginningOffset.y, 0);
		transform.position = new Vector3(transform.position.x, transform.position.y, beginPoint.z);
		if(riseWithSun)
		{
			float percent = MainMechanics.mechanics.timeRemaining/360f;
			transform.position += new Vector3(0, 12.7f*(1-percent));
		}
	}
}
