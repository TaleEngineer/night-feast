﻿using UnityEngine;
using System.Collections;

public class Civillian : Human
{
	public void colorCharacter(bool isFemale)
	{
		float R = Random.Range(0.0f, 1.0f);
		float B = Random.Range(0.0f, 1.0f);
		float G = Random.Range(0.0f, 1.0f);
		
		bool graySleeves = false;
		if(isFemale && Random.Range(0,2) == 1)
		{
			graySleeves = true;
		}
		
		Color color = new Color(R,B,G,1);
		if(isFemale)
		{
			for(int i = 0; i < 7; i++)
			{
				if((i != 4 && i != 6) || !graySleeves)
				{
					bodyParts[i].color = color;
				}
			}
		}
		else
		{
			R = Random.Range(0.0f, 1.0f);
			B = Random.Range(0.0f, 1.0f);
			G = Random.Range(0.0f, 1.0f);
			Color pantsColor = new Color(R,B,G,1);

			for(int i = 0; i < 11; i++)
			{
				if(i < 7)
				{
					bodyParts[i].color = color;
				}
				else
				{
					bodyParts[i].color = pantsColor;
				}
			}
		}
	}

	public override void Meander ()
	{
		int numberOfJumps = UnityEngine.Random.Range (3, 11);
		PathNode currentNode = closestNode;
		for(int i = 0; i < numberOfJumps; i++)
		{
			if(transform.position.y < -2)
			{
				int pathToChoose = UnityEngine.Random.Range (0, currentNode.Neighbors.Count);
				pathToGo.Add(currentNode.Neighbors[pathToChoose]);
				currentNode = currentNode.Neighbors[pathToChoose];
			}
			else
			{
				
				int pathToChoose = UnityEngine.Random.Range (0, currentNode.Neighbors.Count);
				if(currentNode.Neighbors[pathToChoose].nodeType != PathNode.NodeType.ClimbNode)
				{
					pathToGo.Add(currentNode.Neighbors[pathToChoose]);
					currentNode = currentNode.Neighbors[pathToChoose];
				}
			}
		}
		meanderTimer = 5;
	}

	void Start () 
	{
		Physics2D.IgnoreCollision(GetComponent<BoxCollider2D>(), Player.player.GetComponent<BoxCollider2D>());
		animator = GetComponent<Animator>();
		rigidBody = GetComponent<Rigidbody2D>();
		
		//bodyParts[0].transform.position += new Vector3(0, UnityEngine.Random.Range (-0.25f,0.128f),0);
		
		transform.position += new Vector3(0,0,bodyParts[0].transform.position.y);
		
		if(hairOptions.Count > 1)
		{
			int choice = UnityEngine.Random.Range (0, hairOptions.Count);
			
			for(int i = 0; i < hair.Count; i++)
			{
				hair[i].sprite = hairOptions[choice];
			}
		}
		
		if(eyeOptions.Count > 1)
		{
			int choice = UnityEngine.Random.Range (0, eyeOptions.Count);
			
			for(int i = 0; i < eyes.Count; i++)
			{
				eyes[i].sprite = eyeOptions[choice];
			}
		}


	}

	public override void AI ()
	{
		bool playerSpotted = CanSeePlayer();		
		if(currentKnowledgeState == KnowledgeState.Oblivious)
		{
			base.AI ();
		}//OBLIVIOUS
		else if(currentKnowledgeState == KnowledgeState.Curious)
		{
			base.AI ();
		}//CURIOUS
		else if(currentKnowledgeState == KnowledgeState.Suspicious)
		{
			
			if(playerSpotted)
			{
				pathToGo.Clear();
				PathNode nodeToGo = null;
				bool found = false;
				if(Player.player.transform.position.x > transform.position.x)
				{
					for(int i = 0; i < closestNode.Neighbors.Count; i++)
					{
						if(closestNode.Neighbors[i].transform.position.x < Player.player.transform.position.x)
						{
							found = true;
							if(nodeToGo == null)
							{
								nodeToGo = closestNode.Neighbors[i];
							}
							else if(Vector2.Distance(closestNode.Neighbors[i].transform.position, Player.player.transform.position) > 
							        Vector2.Distance(nodeToGo.transform.position, Player.player.transform.position))
							{
								nodeToGo = closestNode.Neighbors[i];
							}
						}
					}
				}
				else
				{
					for(int i = 0; i < closestNode.Neighbors.Count; i++)
					{
						if(closestNode.Neighbors[i].transform.position.x > Player.player.transform.position.x)
						{
							found = true;
							if(nodeToGo == null)
							{
								nodeToGo = closestNode.Neighbors[i];
							}
							else if(Vector2.Distance(closestNode.Neighbors[i].transform.position, Player.player.transform.position) > 
							        Vector2.Distance(nodeToGo.transform.position, Player.player.transform.position))
							{
								nodeToGo = closestNode.Neighbors[i];
							}
						}
					}
				}
				if(!found)
				{
					currentKnowledgeState = KnowledgeState.Knows;
				}
				else
				{
					pathToGo = pathFinder.pathfind(closestNode, nodeToGo);
				}
				if(pathToGo.Count > 0)
				{
					goPath ();
				}
				else
				{
					currentAnimState = AnimState.Idle;
				}
			}
			else
			{

				if(pathToGo.Count > 0)
				{
					goPath();
				}
				else if(turnTimer <= 0)
				{
					turnAround();
				}
				else if(meanderTimer <= 0)
				{
					Meander();
				}
				else
				{
					base.AI();
				}
			}
		}//SUSPICIOUS
		else if (currentKnowledgeState == KnowledgeState.Knows)
		{
			DapperMan closestHunter = null;
			for(int i = 0; i < MainMechanics.mechanics.villagerList.Count; i++)
			{
				Human oneLookedAt = MainMechanics.mechanics.villagerList[i];
				if(oneLookedAt != null)
				{
					float distance = Vector2.Distance (transform.position, oneLookedAt.transform.position);

					if(distance <= 1f)
					{
						if(oneLookedAt.currentKnowledgeState == KnowledgeState.Knows)
						{
							if(oneLookedAt.timeSinceLastSpotting > timeSinceLastSpotting)
							{
								oneLookedAt.lastSpotedMonster = lastSpotedMonster;
								oneLookedAt.timeSinceLastSpotting = timeSinceLastSpotting;
							}
						}
						else
						{
							oneLookedAt.currentKnowledgeState = KnowledgeState.Suspicious;

							if(oneLookedAt.GetComponent<DapperMan>())
							{
								oneLookedAt.OverrideList = oneLookedAt.pathFinder.pathfind(oneLookedAt.closestNode, lastSpotedMonster);
							}
						}
					}

					if(oneLookedAt.GetComponent<DapperMan>())
					{
						if(closestHunter == null)
						{
							closestHunter = oneLookedAt as DapperMan;
						}
						else
						{
							if(distance < Vector2.Distance (closestHunter.transform.position, transform.position) && oneLookedAt.closestNode != closestNode)
							{
								closestHunter = oneLookedAt as DapperMan;
							}
						}
					}
				}
			}

			if(playerSpotted)
			{
				lastSpotedMonster = Player.player.closestNode;
			}
			sprinting = true;
			if(playerSpotted && pathToGo.Count == 0)
			{
				if(closestHunter != null)
				{
					pathToGo = pathFinder.pathfind (closestNode, closestHunter.closestNode);
				}
				else
				{
					Meander();
				}

				if(pathToGo.Count > 0)
				{
					goPath ();
				}
				else
				{
					currentAnimState = AnimState.Idle;
				}
			}
			else
			{
				
				if(pathToGo.Count > 0)
				{
					goPath();
				}
				else if(turnTimer <= 0)
				{
					turnAround();
				}
				else if(meanderTimer <= 0)
				{
					Meander();
				}
				else
				{
					base.AI();
				}
			}
			
		}//KNOWS
		else
		{
			base.AI ();
		}
	}
}
