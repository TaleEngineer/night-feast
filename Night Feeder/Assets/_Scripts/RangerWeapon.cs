﻿using UnityEngine;
using System.Collections;

public class RangerWeapon : Weapon 
{
	public GameObject bulletPrefab;
	int numberOfBullets;
	bool homing = false;
	bool explosive = false;
	float accuracy;
	public GameObject muzzel;

	public AudioClip shotSound;

	public void SetAttack(Human _user, int _damage, float _knockback, string _animName, float attackFrame, int nunBullet, bool _homing, bool _explosive, float _accuracy)
	{
		user = _user;
		damage = _damage;
		knockback = _knockback;
		animationName = _animName;
		hasHit = false;
		hitFrame = attackFrame;
		numberOfBullets = nunBullet;
		homing = _homing;
		explosive = _explosive;
		accuracy = _accuracy;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(user != null)
		{
			if(hasHit == false && user.animator.GetCurrentAnimatorStateInfo(0).IsName(animationName) && user.animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= hitFrame)
			{
				for(int i = 0; i < numberOfBullets; i++)
				{
					Instantiate(bulletPrefab, muzzel.transform.position, Quaternion.Euler(muzzel.transform.rotation.eulerAngles+new Vector3(0,0,Random.Range(0, 45)*(1f-accuracy))));
					MakeSound(shotSound);
				}
				hasHit = true;
			}
		}
	}
}
