﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MainMechanics : MonoBehaviour {

	public bool notForWeb = false;

	static public MainMechanics mechanics;
	public Texture2D clockBase;
	public Texture2D clockFace;

	public Texture2D healthBarBack;
	public Texture2D healthBarBar;
	public Texture2D healthBarFrame;
	public Texture2D healthBarHunger;
	public Texture2D titleTexture;
	public Texture2D startButtonTexture;
	public Texture2D exitButtonTexture;

	
	public Texture2D pageTexture1;
	public Texture2D pageTexture2;
	public Texture2D pageTexture3;
	public Texture2D pageTexture4;
	public Texture2D pageTexture5;
	public Texture2D pageTexture6;
	public Texture2D pageTexture7;

	public GameObject hunterPrefabM;
	public GameObject hunterPrefabF;
	public GameObject civilianPrefabM;
	public GameObject civilianPrefabF;

	public AudioClip toungeStick;
	public AudioClip hockTounge;

	
	public AudioClip lines13;
	public AudioClip lines47;
	public AudioClip lines89;
	public AudioClip lines1012;
	public AudioClip lines1314;
	public AudioClip lines15;

	public float timeRemaining = 360;

	public int kills = 0;

	public AudioSource ambientSound;

	public bool isOnGameLevel = false;

	public enum CauseOfDeath
	{
		NotDead,
		TheSun,
		Villagers,
		Hunger
	}
	public CauseOfDeath hasDiedTo;

	public bool hasWon = false;
	float timeSinceWin = 0f;

	int mainMenuPage = 0;
	float timeSinceHitButton;

	public List<Human> villagerList = new List<Human>();
	// Use this for initialization
	void Awake () 
	{
		mechanics = this;

		for(int i = 0; i < 10; i++)
		{
			float xPlacement = Random.Range(-42f, 42f);
			float yPlacement = Random.Range (-0.25f,0.128f);

			GameObject humanToAdd = Instantiate(civilianPrefabF, new Vector3(xPlacement, yPlacement, 0), Quaternion.Euler (0,0,0)) as GameObject;
			villagerList.Add(humanToAdd.GetComponent<Human>());
			villagerList[villagerList.Count-1].GetComponent<Civillian>().colorCharacter(true);
		}

		for(int i = 0; i < 10; i++)
		{
			float xPlacement = Random.Range(-42f, 42f);
			float yPlacement = Random.Range (-0.25f,0.128f);
			
			GameObject humanToAdd = Instantiate(civilianPrefabM, new Vector3(xPlacement, yPlacement, 0), Quaternion.Euler (0,0,0)) as GameObject;
			villagerList.Add(humanToAdd.GetComponent<Human>());
			villagerList[villagerList.Count-1].GetComponent<Civillian>().colorCharacter(false);
		}

		for(int i = 0; i < 5; i++)
		{
			float xPlacement = Random.Range(-42f, 42f);
			float yPlacement = Random.Range (-0.25f,0.128f);
			
			GameObject humanToAdd = Instantiate(hunterPrefabM, new Vector3(xPlacement, yPlacement, 0), Quaternion.Euler (0,0,0)) as GameObject;
			villagerList.Add(humanToAdd.GetComponent<Human>());
		}

		for(int i = 0; i < 5; i++)
		{
			float xPlacement = Random.Range(-42f, 42f);
			float yPlacement = Random.Range (-0.25f,0.128f);
			
			GameObject humanToAdd = Instantiate(hunterPrefabF, new Vector3(xPlacement, yPlacement, 0), Quaternion.Euler (0,0,0)) as GameObject;
			villagerList.Add(humanToAdd.GetComponent<Human>());
		}


	}
	
	// Update is called once per frame
	void Update () 
	{
		if(isOnGameLevel)
		{
		timeRemaining -= Time.deltaTime;

		if(timeRemaining <= 0)
		{
			burnPlayer();
		}
		}
	}

	void OnGUI()
	{
		if(isOnGameLevel)
		{
		Matrix4x4 baseGUIMatrix = GUI.matrix;

		Rect faceRect = new Rect(Screen.width-Screen.height/20-Screen.height/4, Screen.height/20, Screen.height/4, Screen.height/4);

		GUIUtility.RotateAroundPivot(timeRemaining/2f-90, new Vector2(faceRect.x+faceRect.width/2f, faceRect.y+faceRect.width/2f));

		GUI.DrawTexture(faceRect, clockFace);

		GUI.matrix = baseGUIMatrix;

		Rect bodyRect = new Rect(faceRect.x-faceRect.width*0.165f/2f, faceRect.y-faceRect.width*0.165f/2f, faceRect.width*1.165f,faceRect.width*1.165f);
		GUI.DrawTexture(bodyRect, clockBase);

		Rect healthBarRect = new Rect(Screen.width/100, Screen.height- Screen.height/20 - Screen.width/100, Screen.height/2, Screen.height/20);
		//Rect healthBarRect = new Rect(0,0, Screen.height/4, Screen.height/20);
		GUI.DrawTexture(healthBarRect, healthBarBack);
		Rect healthBarBarRect = new Rect(healthBarRect.x,healthBarRect.y,healthBarRect.width*((float)Player.player.currentHealth/(float)Player.player.maxHealth),healthBarRect.height);
		GUI.DrawTexture(healthBarBarRect, healthBarBar);
		GUI.DrawTexture(healthBarRect, healthBarFrame);

		
		Rect healthBarHungerRect = new Rect(healthBarRect.x,healthBarRect.y,healthBarRect.width*(Player.player.hunger/Player.player.maxHunger),healthBarRect.height);
		GUI.DrawTextureWithTexCoords(healthBarHungerRect, healthBarHunger, 
		                             new Rect(0,0, (Player.player.hunger/Player.player.maxHunger), 1));
		//GUI.DrawTexture(healthBarHungerRect, healthBarHunger);


		if(timeRemaining > 30)
		{
			GUI.color = new Color(0,0,0.15f, 0.25f);
		}
		else
		{
			GUI.color = new Color(1f*(1-timeRemaining/30),1*(1-timeRemaining/30),0.15f*(timeRemaining/30), 0.25f + 0.25f*(1-timeRemaining/30));
				ambientSound.volume = 0;
		}
		GUI.DrawTexture(new Rect(0,0,Screen.width,Screen.height), Texture2D.whiteTexture);
		GUI.color = Color.white;

		if(hasWon)
		{
			timeSinceWin += Time.deltaTime;
			GUI.color = new Color(0,0,0, timeSinceWin);
			GUI.DrawTexture(new Rect(0,0,Screen.width,Screen.height), Texture2D.whiteTexture);
			GUI.color = Color.white;
			VictoryScreen(timeSinceWin);
		}
		else
		{
			AnimatorStateInfo currentAnim = Player.player.animator.GetCurrentAnimatorStateInfo(0);
			if(hasDiedTo == CauseOfDeath.TheSun)
			{
				
				if(currentAnim.IsName("Die"))
				{
					GUI.color = new Color(1,1,0, currentAnim.normalizedTime);
					GUI.DrawTexture(new Rect(0,0,Screen.width,Screen.height), Texture2D.whiteTexture);
					GUI.color = Color.white;
				}
			}
			else if(hasDiedTo == CauseOfDeath.Villagers)
			{
				if(currentAnim.IsName("Die"))
				{
					GUI.color = new Color(0,0,0, currentAnim.normalizedTime);
					GUI.DrawTexture(new Rect(0,0,Screen.width,Screen.height), Texture2D.whiteTexture);
					GUI.color = Color.white;
				}
			}
			else if(hasDiedTo == CauseOfDeath.Hunger)
			{
				if(currentAnim.IsName("Die"))
				{
					if(kills == 0)
					{
						GUI.color = new Color(1,1,1, currentAnim.normalizedTime);
					}
					else
					{
						GUI.color = new Color(0,0,0, currentAnim.normalizedTime);
					}
					GUI.DrawTexture(new Rect(0,0,Screen.width,Screen.height), Texture2D.whiteTexture);
					GUI.color = Color.white;
				}
			}

			if(hasDiedTo != CauseOfDeath.NotDead && currentAnim.IsName("Die"))
			{
				GameOverScreen(currentAnim.normalizedTime);
			}
		}
	}
		else//MAIN MENU
		{
			var labelStyle = GUI.skin.GetStyle ("Label");
			labelStyle.fontSize = Screen.height/20;
			labelStyle.alignment = TextAnchor.MiddleCenter;

			if(mainMenuPage == 0)
			{
				Rect titleRect = new Rect(Screen.width/2 - Screen.height/4, Screen.height/2-Screen.height/8, Screen.height/2, Screen.height/4); 
				GUI.DrawTexture(titleRect, titleTexture);

				Rect startButtonRect = new Rect(Screen.width/2-Screen.height/8, titleRect.y+titleRect.height + Screen.height/16, Screen.height/4, Screen.height/8);

				if(startButtonRect.Contains(new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
				{
					GUI.color = new Color(0.5f, 0.5f, 0.5f, 1);
					if(Input.GetMouseButtonUp(0))
					{
						mainMenuPage = 1;
					}
				}
				GUI.DrawTexture(startButtonRect, startButtonTexture);
				
				GUI.color = Color.white;

				if(notForWeb)
				{
					Rect exitButtonRect = new Rect(Screen.width/2-Screen.height/8, startButtonRect.y+startButtonRect.height, Screen.height/4, Screen.height/8);
					
					if(exitButtonRect.Contains(new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
					{
						GUI.color = new Color(0.5f, 0.5f, 0.5f, 1);
						if(Input.GetMouseButtonUp(0))
						{
							Application.Quit();
						}
					}
					GUI.DrawTexture(exitButtonRect, exitButtonTexture);

				}
				GUI.color = Color.white;
			}
			else if(mainMenuPage == 1)
			{
				if(timeSinceHitButton <= 0)
				{
					ambientSound.Stop();
					ambientSound.volume = 1;
				}
				timeSinceHitButton += Time.deltaTime;
				GUI.color = new Color(0,0,0,timeSinceHitButton);
				GUI.DrawTexture(new Rect(0,0, Screen.width, Screen.height), Texture2D.whiteTexture);
				GUI.color = Color.white;

				if(timeSinceHitButton >= 1)
				{
					mainMenuPage = 2;
					timeSinceHitButton = 0;
				}
			}
			else if(mainMenuPage == 2)
			{
				if(timeSinceHitButton <= 0)
				{
					ambientSound.Stop();
					ambientSound.clip = lines13;
					ambientSound.Play();
					ambientSound.volume = 1;
					ambientSound.loop = false;
				}
				GUI.color = new Color(0,0,0, 1);
				GUI.DrawTexture(new Rect(0,0, Screen.width, Screen.height), Texture2D.whiteTexture);
				GUI.color = Color.white;

				if(timeSinceHitButton <= 1)
				{
					GUI.Label(new Rect(0,0, Screen.width, Screen.height), "Master!");
				}
				else if(timeSinceHitButton <= 2.6f)
				{
					GUI.Label(new Rect(0,0, Screen.width, Screen.height), "You have Awoken!");
				}
				else if(timeSinceHitButton <= 7)
				{
					GUI.Label(new Rect(0,0, Screen.width, Screen.height), "Please, stay here! I will be back with the book!");
				}

				
				timeSinceHitButton += Time.deltaTime/2;
				if(timeSinceHitButton >= 10 || (timeSinceHitButton > 1 && Input.anyKeyDown))
				{
					mainMenuPage = 3;
					timeSinceHitButton = 0;
				}
			}
			else if(mainMenuPage == 3)
			{
				if(timeSinceHitButton <= 0)
				{
					ambientSound.Stop();
					ambientSound.clip = lines47;
					ambientSound.Play();
					ambientSound.volume = 1;
				}
				GUI.color = new Color(0,0,0, 1);
				GUI.DrawTexture(new Rect(0,0, Screen.width, Screen.height), Texture2D.whiteTexture);
				GUI.color = Color.white;
				
				if(timeSinceHitButton <= 2.5f)
				{
					GUI.Label(new Rect(0,0, Screen.width, Screen.height), "I have returned, Master!");
				}
				else if(timeSinceHitButton <= 4.8f)
				{
					GUI.Label(new Rect(0,0, Screen.width, Screen.height), "Please, read the book!");
				}
				else if(timeSinceHitButton <= 9f)
				{
					GUI.Label(new Rect(0,0, Screen.width, Screen.height), "It... It was written by... well...");
				}
				else if(timeSinceHitButton <= 10f)
				{
					GUI.Label(new Rect(0,0, Screen.width, Screen.height), "You!");
				}
				else if(timeSinceHitButton <= 16.8f)
				{
					GUI.Label(new Rect(0,0, Screen.width, Screen.height), "Or rather, the you you were before you became the you you are now.");
				}
				else if(timeSinceHitButton <= 17.4f)
				{
					GUI.Label(new Rect(0,0, Screen.width, Screen.height), "I think.");
				}
				
				timeSinceHitButton += Time.deltaTime/2;
				if(timeSinceHitButton >= 18 || (timeSinceHitButton > 1 && Input.anyKeyDown))
				{
					mainMenuPage = 4;
					timeSinceHitButton = 0;
				}

			}
			else if(mainMenuPage == 4)
			{
				if(timeSinceHitButton <= 0)
				{
					ambientSound.Stop();
					ambientSound.clip = lines89;
					ambientSound.Play();
					ambientSound.volume = 1;
				}

				
				GUI.color = new Color(0,0,0, 1);
				GUI.DrawTexture(new Rect(0,0, Screen.width, Screen.height), Texture2D.whiteTexture);
				GUI.color = Color.white;
				
				if(timeSinceHitButton <= 5f)
				{
					GUI.Label(new Rect(0,0, Screen.width, Screen.height), "Look! Look! Each color is a different you!");
				}
				else if(timeSinceHitButton <= 9f)
				{
					GUI.Label(new Rect(0,0, Screen.width, Screen.height), "You can ignore Green, though. He's... He's an idiot.");
				}
				else
				{
					GUI.DrawTexture(new Rect(Screen.width/2-Screen.height*0.65f, 0, Screen.height*1.3f, Screen.height), pageTexture1); 
				}
				
				timeSinceHitButton += Time.deltaTime/2;
				if((Input.anyKeyDown && timeSinceHitButton > 9)  || (timeSinceHitButton > 1 && Input.anyKeyDown))
				{
					mainMenuPage = 5;
					timeSinceHitButton = 0;
				}
			}
			else if(mainMenuPage == 5)
			{
				if(timeSinceHitButton <= 0)
				{
					ambientSound.Stop ();
					ambientSound.volume = 1;
				}
				
				
				GUI.color = new Color(0,0,0, 1);
				GUI.DrawTexture(new Rect(0,0, Screen.width, Screen.height), Texture2D.whiteTexture);
				GUI.color = Color.white;
				
				GUI.DrawTexture(new Rect(Screen.width/2-Screen.height*0.65f, 0, Screen.height*1.3f, Screen.height), pageTexture2);
				
				timeSinceHitButton += Time.deltaTime/2;
				if(Input.anyKeyDown && timeSinceHitButton > 1)
				{
					mainMenuPage = 6;
					timeSinceHitButton = 0;
				}
			}
			else if(mainMenuPage == 6)
			{
				if(timeSinceHitButton <= 0)
				{
					ambientSound.volume = 1;
				}
				
				
				GUI.color = new Color(0,0,0, 1);
				GUI.DrawTexture(new Rect(0,0, Screen.width, Screen.height), Texture2D.whiteTexture);
				GUI.color = Color.white;
				
				GUI.DrawTexture(new Rect(Screen.width/2-Screen.height*0.65f, 0, Screen.height*1.3f, Screen.height), pageTexture3);
				
				timeSinceHitButton += Time.deltaTime/2;
				if(Input.anyKeyDown && timeSinceHitButton > 1)
				{
					mainMenuPage = 7;
					timeSinceHitButton = 0;
				}
			}
			else if(mainMenuPage == 7)
			{
				if(timeSinceHitButton <= 0)
				{
					ambientSound.volume = 1;
				}
				
				
				GUI.color = new Color(0,0,0, 1);
				GUI.DrawTexture(new Rect(0,0, Screen.width, Screen.height), Texture2D.whiteTexture);
				GUI.color = Color.white;
				
				GUI.DrawTexture(new Rect(Screen.width/2-Screen.height*0.65f, 0, Screen.height*1.3f, Screen.height), pageTexture4);
				
				timeSinceHitButton += Time.deltaTime/2;
				if(Input.anyKeyDown && timeSinceHitButton > 1)
				{
					mainMenuPage = 8;
					timeSinceHitButton = 0;
				}
			}
			else if(mainMenuPage == 8)
			{
				if(timeSinceHitButton <= 0)
				{
					ambientSound.volume = 1;
				}
				
				
				GUI.color = new Color(0,0,0, 1);
				GUI.DrawTexture(new Rect(0,0, Screen.width, Screen.height), Texture2D.whiteTexture);
				GUI.color = Color.white;
				
				GUI.DrawTexture(new Rect(Screen.width/2-Screen.height*0.65f, 0, Screen.height*1.3f, Screen.height), pageTexture5);
				
				timeSinceHitButton += Time.deltaTime/2;
				if(Input.anyKeyDown && timeSinceHitButton > 1)
				{
					mainMenuPage = 9;
					timeSinceHitButton = 0;
				}
			}
			else if(mainMenuPage == 9)
			{
				if(timeSinceHitButton <= 0)
				{
					ambientSound.volume = 1;
				}
				
				
				GUI.color = new Color(0,0,0, 1);
				GUI.DrawTexture(new Rect(0,0, Screen.width, Screen.height), Texture2D.whiteTexture);
				GUI.color = Color.white;
				
				GUI.DrawTexture(new Rect(Screen.width/2-Screen.height*0.65f, 0, Screen.height*1.3f, Screen.height), pageTexture6);
				
				timeSinceHitButton += Time.deltaTime/2;
				if(Input.anyKeyDown && timeSinceHitButton > 1)
				{
					mainMenuPage = 10;
					timeSinceHitButton = 0;
				}
			}else if(mainMenuPage == 10)
			{
				if(timeSinceHitButton <= 0)
				{
					ambientSound.volume = 1;
				}
				
				
				GUI.color = new Color(0,0,0, 1);
				GUI.DrawTexture(new Rect(0,0, Screen.width, Screen.height), Texture2D.whiteTexture);
				GUI.color = Color.white;
				
				GUI.DrawTexture(new Rect(Screen.width/2-Screen.height*0.65f, 0, Screen.height*1.3f, Screen.height), pageTexture7);
				
				timeSinceHitButton += Time.deltaTime/2;
				if(Input.anyKeyDown && timeSinceHitButton > 1)
				{
					mainMenuPage = 11;
					timeSinceHitButton = 0;
				}
			}
			else if(mainMenuPage == 11)
			{
				if(timeSinceHitButton <= 0)
				{
					ambientSound.volume = 1;
					ambientSound.Stop();
					ambientSound.clip = (lines1012);
					ambientSound.Play ();
				}
				
				
				GUI.color = new Color(0,0,0, 1);
				GUI.DrawTexture(new Rect(0,0, Screen.width, Screen.height), Texture2D.whiteTexture);
				GUI.color = Color.white;

				if(timeSinceHitButton <= 3f)
				{
					GUI.Label(new Rect(0,0, Screen.width, Screen.height), "Master! I completely forgot!");
				}
				else if(timeSinceHitButton <= 4.5f)
				{
					GUI.Label(new Rect(0,0, Screen.width, Screen.height), "You need to Feast!");
				}
				else if(timeSinceHitButton <= 8.5f)
				{
					GUI.Label(new Rect(0,0, Screen.width, Screen.height), "Quickly, Master, before you die of the Hunger!");
				}
				
				timeSinceHitButton += Time.deltaTime/2;
				if(timeSinceHitButton > 8.5f || (timeSinceHitButton > 1 && Input.anyKeyDown))
				{
					mainMenuPage = 12;
					timeSinceHitButton = 0;
				}
			}
			else if(mainMenuPage == 12)
			{
				if(timeSinceHitButton <= 0)
				{
					ambientSound.volume = 1;
					ambientSound.Stop();
					ambientSound.clip = lines1314;
					ambientSound.Play ();
				}
				
				
				GUI.color = new Color(0,0,0, 1);
				GUI.DrawTexture(new Rect(0,0, Screen.width, Screen.height), Texture2D.whiteTexture);
				GUI.color = Color.white;
				
				if(timeSinceHitButton <= 7.9f)
				{
					GUI.Label(new Rect(0,0, Screen.width, Screen.height), "Just remember to return before THE BURNING HATE rises from beyond the Horizon!");
				}
				else if(timeSinceHitButton <= 11f)
				{
					GUI.Label(new Rect(0,0, Screen.width, Screen.height), "Oh, and look out for the Hunters too.");
				}

				timeSinceHitButton += Time.deltaTime/2;
				if(timeSinceHitButton > 12f || (timeSinceHitButton > 1 && Input.anyKeyDown))
				{
					mainMenuPage = 13;
					timeSinceHitButton = 0;
					ambientSound.Stop();
				}
			}
			else if(mainMenuPage == 13)
			{
				if(timeSinceHitButton <= 0)
				{
					ambientSound.volume = 1;
					ambientSound.Stop();
					ambientSound.clip = lines15;
					ambientSound.Play();
				}
				
				
				GUI.color = new Color(0,0,0, 1);
				GUI.DrawTexture(new Rect(0,0, Screen.width, Screen.height), Texture2D.whiteTexture);
				GUI.color = Color.white;
				
				if(timeSinceHitButton <= 3f)
				{
					GUI.Label(new Rect(0,0, Screen.width, Screen.height), "Happy Feasting Master!");
				}
				
				timeSinceHitButton += Time.deltaTime/2;
				if(timeSinceHitButton > 4f || (timeSinceHitButton > 1 && Input.anyKeyDown))
				{
					ambientSound.Stop();
					Application.LoadLevel(1);
				}
			}
		}
	}

	void VictoryScreen(float timer)
	{
		var labelStyle = GUI.skin.GetStyle ("Label");
		labelStyle.fontSize = Screen.height/20;
		labelStyle.alignment = TextAnchor.MiddleCenter;
		
		string message = "You have Feasted well this night";
		Color colorToUse =  new Color(1,1,1, (timer/2f)-1);
		
		
		GUI.color = colorToUse;
		
		GUIContent buttonContent = new GUIContent(message);
		Vector2 textSize = labelStyle.CalcSize(buttonContent);
		Rect gameOverRect = new Rect(Screen.width/2 - textSize.x/2, Screen.height/2 - textSize.y/2,textSize.x, textSize.y);
		GUI.Label (gameOverRect, message);
		
		
		
		labelStyle.fontSize = Screen.height/40;
		
		if(kills == 0)
		{
			message = "Yet you ate nothing!? How did you do this!?";
		}
		else if(kills == 1)
		{
			message = "You have only eaten but one thing!? How!?";
		}
		else
		{
			message = "A delicious Feast of "+kills+" humans";
		}
		buttonContent = new GUIContent(message);
		textSize = labelStyle.CalcSize(buttonContent);
		gameOverRect = new Rect(Screen.width/2 - textSize.x/2, gameOverRect.y+gameOverRect.height,textSize.x, textSize.y);
		GUI.Label (gameOverRect, message);


		message = "Feast Again?";
		buttonContent = new GUIContent(message);
		textSize = labelStyle.CalcSize(buttonContent);
		gameOverRect = new Rect(Screen.width/2 - textSize.x/2, gameOverRect.y+gameOverRect.height+Screen.height/20,textSize.x, textSize.y);
		if(gameOverRect.Contains(new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
		{
			GUI.color = new Color(0.5f, 0.5f, 0.5f, timeRemaining);

			if(Input.GetMouseButtonUp(0))
			{
				Application.LoadLevel (1);
			}
		}
		GUI.Label (gameOverRect, message);
		
		GUI.color = colorToUse;

		if(notForWeb)
		{
			message = "Exit?";
			buttonContent = new GUIContent(message);
			textSize = labelStyle.CalcSize(buttonContent);
			gameOverRect = new Rect(Screen.width/2 - textSize.x/2, gameOverRect.y+gameOverRect.height+Screen.height/20,textSize.x, textSize.y);
			if(gameOverRect.Contains(new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{
				GUI.color = new Color(0.5f, 0.5f, 0.5f, timeRemaining);
				
				if(Input.GetMouseButtonUp(0))
				{
					Application.Quit();
				}
			}
			GUI.Label (gameOverRect, message);
		}


		GUI.color = Color.white;
	}

	void GameOverScreen(float normalizedFrame)
	{
		var labelStyle = GUI.skin.GetStyle ("Label");
		labelStyle.fontSize = Screen.height/20;
		labelStyle.alignment = TextAnchor.MiddleCenter;

		string message = "UNKNOWN_DEATH_ERROR";
		Color colorToUse = Color.blue;
		if(hasDiedTo == CauseOfDeath.TheSun)
		{
			message = "Your soul has been consumed by \n" +
				"THE BURNING HATE";
			colorToUse = new Color(1,0,0, (normalizedFrame/2f)-1);
		}
		else if(hasDiedTo == CauseOfDeath.Villagers)
		{
			message = "Your body has been broken by \n" +
				"HUNTER SCUM";
			colorToUse = new Color(1,1,1, (normalizedFrame/2f)-1);
		}
		else if(hasDiedTo == CauseOfDeath.Hunger && kills == 0)
		{
			message = "You have refused to FEAST \n" +
				"and died of The Hunger";
			colorToUse = new Color(0,0,0, (normalizedFrame/2f)-1);
		}
		else if(hasDiedTo == CauseOfDeath.Hunger)
		{
			message = "You have died from \n" +
				"THE HUNGER";
			colorToUse = new Color(1,1,1, (normalizedFrame/2f)-1);
		}

		
		GUI.color = colorToUse;

		GUIContent buttonContent = new GUIContent(message);
		Vector2 textSize = labelStyle.CalcSize(buttonContent);
		Rect gameOverRect = new Rect(Screen.width/2 - textSize.x/2, Screen.height/2 - textSize.y/2,textSize.x, textSize.y);
		GUI.Label (gameOverRect, message);
		
		
		
		labelStyle.fontSize = Screen.height/40;

		if(kills == 0 && hasDiedTo == CauseOfDeath.Hunger)
		{
			message = "How noble of you";
		}
		else if(kills == 1 && hasDiedTo == CauseOfDeath.Hunger)
		{
			message = "One human is not enough";
		}
		else if(hasDiedTo == CauseOfDeath.Hunger)
		{
			message = "And the "+kills+" humans you Feasted on did not help";
		}
		else if(kills == 0)
		{
			message = "And you failed to Feast on anything";
		}
		else if(kills == 1)
		{
			message = "But at least you Feasted on a human first";
		}
		else
		{
			message = "But at least you Feasted on "+kills+" humans";
		}
		buttonContent = new GUIContent(message);
		textSize = labelStyle.CalcSize(buttonContent);
		gameOverRect = new Rect(Screen.width/2 - textSize.x/2, gameOverRect.y+gameOverRect.height,textSize.x, textSize.y);
		GUI.Label (gameOverRect, message);

		message = "Try Again?";
		buttonContent = new GUIContent(message);
		textSize = labelStyle.CalcSize(buttonContent);
		gameOverRect = new Rect(Screen.width/2 - textSize.x/2, gameOverRect.y+gameOverRect.height+Screen.height/20,textSize.x, textSize.y);
		if(gameOverRect.Contains(new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
		{
			GUI.color = new Color(colorToUse.r/2f, colorToUse.b/2f, colorToUse.g/2f, normalizedFrame/2f-1);

			if(Input.GetMouseButtonUp(0))
			{
				Application.LoadLevel (1);
			}
		}
		GUI.Label (gameOverRect, message);
		
		GUI.color = colorToUse;

		if(notForWeb)
		{
			message = "Exit?";
			buttonContent = new GUIContent(message);
			textSize = labelStyle.CalcSize(buttonContent);
			gameOverRect = new Rect(Screen.width/2 - textSize.x/2, gameOverRect.y+gameOverRect.height+Screen.height/20,textSize.x, textSize.y);
			if(gameOverRect.Contains(new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y)))
			{
				GUI.color = new Color(0.5f, 0.5f, 0.5f, timeRemaining);
				
				if(Input.GetMouseButtonUp(0))
				{
					Application.Quit();
				}
			}
			GUI.Label (gameOverRect, message);
		}

		GUI.color = Color.white;

	}

	public void HasWon()
	{
		hasWon = true;
	}

	void burnPlayer()
	{
		Player.player.currentHealth = 0;
		if(hasDiedTo == CauseOfDeath.NotDead)
		{
			hasDiedTo = CauseOfDeath.TheSun;
		}
	}
}
