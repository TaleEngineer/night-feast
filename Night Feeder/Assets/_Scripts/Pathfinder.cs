﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Pathfinder
{
	List<Node> openList;
	List<Node> closedList;
	Node currentNode;
	Node startNode;
	Node destinationNode;
	
	//int movementCost = 5;
	
	bool pathFound;
	
	class Node
	{
		public PathNode pathNode;
		public Node parentNode;
		public float gValue;
		public float hValue;
		public float fValue;
		
		public void Set(PathNode node, float g_Value, float h_Value, Node parent)
		{
			pathNode = node;
			parentNode = parent;
			gValue = g_Value;
			hValue = h_Value;
			fValue = hValue+gValue;
		}
	}
	
	public List<PathNode> pathfind(PathNode closestNode, PathNode targetNode)
	{
		for(int i = 0; i < closestNode.Neighbors.Count; i++)
		{
			if(closestNode.Neighbors[i] == targetNode)
			{
				List<PathNode> toReturn = new List<PathNode>();
				toReturn.Add(targetNode);
				return toReturn;
			}
		}
		
		pathFound = false;
		
		if(closestNode == targetNode || targetNode == null)
		{
			return new List<PathNode>();
		}
		
		startNode = new Node();
		destinationNode = new Node();
		openList = new List<Node>();
		closedList = new List<Node>();
		
		startNode.pathNode = closestNode;
		destinationNode.pathNode = targetNode;
		currentNode = startNode;
		
		
		openList.Add(currentNode);
		
		for(int i = 0; i < 2000; i+= 0)
		{
			bool weStuckYo = false;
			if (pathFound == false)
			{
				weStuckYo = findPath();
			}
			else if (pathFound == true)
			{
				currentNode = destinationNode;
				return traceBack();
			}
			
			if (weStuckYo == true)
			{
				return new List<PathNode>();
			}
		}
		
		return null;
	}
	
	bool findPath()
	{
		closedList.Add(currentNode);
		openList.Remove(currentNode);

		for(int i = 0; i < currentNode.pathNode.Neighbors.Count; i++)
		{
			if(currentNode.pathNode.Neighbors[i] == destinationNode.pathNode)
			{
				pathFound = true;
				destinationNode.parentNode = currentNode;
				return false;
			}
			
			checkNodeDirection(currentNode.pathNode.Neighbors[i]);
		}
		
		if (openList.Count > 0)
		{
			currentNode = openList[0];
			for(int i = 0; i < openList.Count; i++)
			{
				if(openList[i].fValue < currentNode.fValue)
				{
					currentNode = openList[i];
				}
			}
			return false;
		}
	
	return true;
	}
	
	void checkNodeDirection(PathNode nodeToCheck)
	{
		bool inOpenList = false;
		bool inClosedList = false;
		
		float movementCost = Vector3.Distance(currentNode.pathNode.transform.position, nodeToCheck.transform.position);
		
		for (int i = 0; i < openList.Count; i++)
		{
			if (nodeToCheck == openList[i].pathNode)
			{
				inOpenList = true;
				if (openList[i].gValue > currentNode.gValue + movementCost)
				{
					openList[i].gValue = currentNode.gValue + movementCost;
					openList[i].parentNode = currentNode;
				}
				break;
			}
		}
		if (inOpenList == false)
		{
			for (int i = 0; i < closedList.Count; i++)
			{
				if (closedList[i].pathNode == nodeToCheck)
				{
					inClosedList = true;
					break;
				}
			}
			if (inClosedList == false)
			{
				Node node = new Node();
				node.Set(nodeToCheck, movementCost + currentNode.gValue, Vector3.Distance(nodeToCheck.transform.position,destinationNode.pathNode.transform.position), currentNode);
				openList.Add(node);
			}
		}
	}
	
	
	List<PathNode> traceBack()
	{
		List<PathNode> destinations = new List<PathNode>();
		for (int i = 0; i < 1; )
		{
			if (currentNode.parentNode != null)
			{
				currentNode = currentNode.parentNode;
				destinations.Add(currentNode.pathNode);
			}
			else
			{
				destinations.Reverse();
				destinations.RemoveAt(0);
				return destinations;
			}
		}
		return null;
	}

}
