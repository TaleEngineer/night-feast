﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player : MonoBehaviour 
{
	public int currentHealth = 1000;
	public int maxHealth = 1000;

	public PathNode closestNode;

	public enum AnimState
	{
		Idle, //0
		Walk, //1
		Run, //2
		Jump, //3
		Fall, //4
		Tounge, //5
		ToungeRetract, //6
		Die //7
	}

	public List<SpriteRenderer> bodyParts = new List<SpriteRenderer>();

	public AnimState currentAnimState = AnimState.Idle;

	public static Player player;
	public Animator animator;

	bool hasJumped = false;

	public ToungePiece mostRecentPiece;
	public GameObject spewPoint;
	public GameObject toungeTipPrefab;
	public GameObject toungePiecePrefab;
	public GameObject bloodPrefab;

	public bool readyToPulse = true;

	public float maxHunger = 100f;
	public float hunger = 100f;

	float hungerTimer = 0;

	AudioSource audioSource;

	public AudioClip death;
	public AudioClip slurp;
	public AudioClip pain;

	public void MakeSound(AudioClip sound)
	{
		if(MainMechanics.mechanics.isOnGameLevel)
		{
		audioSource.pitch = Random.Range(0.875f, 1.2f);
		audioSource.PlayOneShot(sound);
		}
	}

	public void TakeDamage(int damage)
	{
		currentHealth -= damage;
		if(currentHealth < 0)
		{
			currentHealth = 0;
			currentAnimState = AnimState.Die;
			MainMechanics.mechanics.hasDiedTo = MainMechanics.CauseOfDeath.Villagers;
		}
		else
		{
			MakeSound(pain);
		}
		if(ToungeTip.theTip != null)
		{
			ToungeTip.theTip.clearTarget();
			currentAnimState = AnimState.Idle;
			DestroyTounge();
		}
		hungerTimer = 0;
	}

	public void DestroyTounge()
	{
		while(mostRecentPiece != null)
		{
			ToungePiece toDelete = mostRecentPiece;
			if(mostRecentPiece.parent != null)
			{
				mostRecentPiece = mostRecentPiece.parent;
			}
			else
			{
				mostRecentPiece = null;
			}
			if(toDelete.transform.localScale.y == 2)
			{
				Instantiate(bloodPrefab, toDelete.transform.position, Quaternion.Euler(0,0,0));
			}
			Destroy(toDelete.gameObject);
		}
	}

	void Feast()
	{
		currentHealth += 10;
		if(currentHealth > maxHealth)
		{
			currentHealth = maxHealth;
		}
		
		hunger += 10;
		if(hunger > maxHunger)
		{
			hunger = maxHunger;
		}
	}
	
	// Use this for initialization
	void Awake () 
	{
		animator = GetComponent<Animator>();
		player = this;
		audioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(MainMechanics.mechanics.isOnGameLevel)
		{
		AnimatorStateInfo currentAnim = animator.GetCurrentAnimatorStateInfo(0);
		Rigidbody2D rigidBody = GetComponent<Rigidbody2D>();

		if(hunger > 0)
		{
			hunger -= Time.deltaTime;
			if(hunger < 0)
			{
				hunger = 0;
			}
		}
		else
		{
			hungerTimer += Time.deltaTime;
			while(hungerTimer >= 0.025f)
			{
				currentHealth --;
				if(currentHealth < 0)
				{
					currentHealth = 0;
					MainMechanics.mechanics.hasDiedTo = MainMechanics.CauseOfDeath.Hunger;
					currentAnimState = AnimState.Die;
				}
				hungerTimer-= 0.025f;
			}
		}

		if(MainMechanics.mechanics.hasWon)
		{

		}
		else if(currentHealth <= 0)
		{
			currentAnimState = AnimState.Die;
		}
		else if(currentAnimState == AnimState.Jump && hasJumped == false)
		{
			if(currentAnim.IsName("Jump") && currentAnim.normalizedTime >= 0.08695f)
			{
				rigidBody.velocity = new Vector2(rigidBody.velocity.x, 10);
				hasJumped = true;
			}
		}
		else if(currentAnimState == AnimState.Tounge)
		{
			if(mostRecentPiece != null && mostRecentPiece.transform.localScale.y == 2)
			{
				Feast();
			}
			if(Input.GetButtonDown("Tounge"))
			{
				readyToPulse = true;
			}
			if(ToungeTip.theTip == null)
			{
				if((currentAnim.IsName("ToungeLaunch")&& currentAnim.normalizedTime >= 0.5f) || currentAnim.IsName("ToungeSustain"))
				{
					GameObject tipGO = Instantiate(toungeTipPrefab, spewPoint.transform.position+spewPoint.transform.forward*0.088f, spewPoint.transform.rotation) as GameObject;
					tipGO.transform.localScale = transform.localScale;
					GameObject pieceGO = Instantiate(toungePiecePrefab, spewPoint.transform.position+spewPoint.transform.forward*0.01f, spewPoint.transform.rotation) as GameObject;
					ToungeTip tip = tipGO.GetComponent<ToungeTip>();
					mostRecentPiece = pieceGO.GetComponent<ToungePiece>();
					tip.child = mostRecentPiece;
					mostRecentPiece.parent = tip;
					MakeSound(MainMechanics.mechanics.hockTounge);
				}
			}
			else if(ToungeTip.theTip.retracting == false)
			{
				ToungeTip.theTip.UpdatePosition();
				int i = 0;
				while(Vector2.Distance(mostRecentPiece.transform.position, spewPoint.transform.position) > 0.02f)
				{
					if((mostRecentPiece.transform.position.x-spewPoint.transform.position.x)*transform.localScale.x <= 0)
					{
						break;
					}
					i++;
					GameObject pieceGO = Instantiate (toungePiecePrefab, new Vector3(mostRecentPiece.transform.position.x - 0.01f*transform.localScale.x, spewPoint.transform.position.y,0), spewPoint.transform.rotation) as GameObject;
					ToungePiece piece = pieceGO.GetComponent<ToungePiece>();
					piece.parent = mostRecentPiece;
					mostRecentPiece.child = piece;
					mostRecentPiece = piece;
					if(i > 1000)
					{
						break;
					}
				}
				mostRecentPiece.UpdateSize();


				if(mostRecentPiece != null && (mostRecentPiece.transform.position.x-spewPoint.transform.position.x)*transform.localScale.x < 0)
				{
					while((mostRecentPiece.transform.position.x-spewPoint.transform.position.x)*transform.localScale.x < 0)
					{
						if(mostRecentPiece != null && mostRecentPiece.transform.localScale.y == 2)
						{
							Feast();
						}

						bool itsOver = false;
						ToungePiece toDelete = mostRecentPiece;
						if(toDelete.parent != null)
						{
							mostRecentPiece = toDelete.parent;
							mostRecentPiece.child = null;
						}
						else
						{
							currentAnimState = AnimState.Idle;
							mostRecentPiece = null;
							itsOver = true;
						}

						if(toDelete == ToungeTip.theTip)
						{
							ToungeTip.theTip.clearTarget();
						}
						Destroy(toDelete.gameObject);
						if(itsOver)
						{
							break;
						}
					}
				}
			}
			else if(ToungeTip.theTip.retracting)
			{
				mostRecentPiece.UpdatePosition();

				if(mostRecentPiece != null && (mostRecentPiece.transform.position.x-spewPoint.transform.position.x)*transform.localScale.x <= 0)
				{
					while((mostRecentPiece.transform.position.x-spewPoint.transform.position.x)*transform.localScale.x <= 0)
					{
						if(mostRecentPiece != null && mostRecentPiece.transform.localScale.y == 2)
						{
							Feast();
						}

						bool itsOver = false;
						ToungePiece toDelete = mostRecentPiece;
						if(toDelete.parent != null)
						{
							mostRecentPiece = toDelete.parent;
							mostRecentPiece.child = null;
						}
						else
						{
							currentAnimState = AnimState.Idle;
							mostRecentPiece = null;
							itsOver = true;
						}
						Destroy(toDelete.gameObject);
						if(itsOver)
						{
							break;
						}
					}
				}
				if(mostRecentPiece != null)
				{
					mostRecentPiece.UpdateSize();
				}
			}

			if(Input.GetButtonDown("Jump"))
			{
				ToungeTip.theTip.retracting = true;
			}

		}
		else
		{
			if(rigidBody.velocity.y < 0f)
			{
				currentAnimState = AnimState.Fall;
			}
			if(currentAnimState == AnimState.Fall)
			{
				if(rigidBody.velocity.y == 0)
				{
					hasJumped = false;
					currentAnimState = AnimState.Idle;
				}
			}

			if(Input.GetButtonDown("Tounge"))
			{
				currentAnimState = AnimState.Tounge;
			}
			else if(Input.GetButtonDown("Jump") && currentAnimState != AnimState.Fall)
			{
				currentAnimState = AnimState.Jump;
			}
			else if(Input.GetAxisRaw("Horizontal") != 0)
			{
				float dir = Input.GetAxisRaw("Horizontal");
				int negative = -1;
				if(dir < 0)
				{
					negative = 1;
					transform.localScale = new Vector3(-1,1,1);
				}
				else
				{
					transform.localScale = new Vector3(1,1,1);
				}

				if(Input.GetButton("Sprint"))
				{
					rigidBody.velocity = new Vector2(dir*6, rigidBody.velocity.y);
					transform.position += new Vector3(dir*0.001f,0);
					transform.Find("Body").rotation = Quaternion.Euler(new Vector3(0,0,10*negative));
					if(currentAnimState != AnimState.Jump && currentAnimState != AnimState.Fall)
					{
						currentAnimState = AnimState.Run;
					}
				}
				else
				{
					rigidBody.velocity = new Vector2(dir*3, rigidBody.velocity.y);
					transform.FindChild("Body").rotation = Quaternion.Euler(new Vector3(0,0,5*negative));
					if(currentAnimState != AnimState.Jump && currentAnimState != AnimState.Fall)
					{
						currentAnimState = AnimState.Walk;
					}
				}


			}
			else
			{
				if(currentAnimState != AnimState.Jump && currentAnimState != AnimState.Fall)
				{
					currentAnimState = AnimState.Idle;
				}
				transform.rotation = Quaternion.Euler(new Vector3(0,0, 0));
			}
		}
		if(currentAnimState == AnimState.Idle)
		{
			if(currentAnim.IsName("Idle"))
			{
				animator.SetInteger("AnimationState", -1);
			}
			else
			{
				animator.SetInteger("AnimationState", 0);
			}
		}
		else if(currentAnimState == AnimState.Walk)
		{
			if(currentAnim.IsName("Walk"))
			{
				animator.SetInteger("AnimationState", -1);
			}
			else
			{
				animator.SetInteger("AnimationState", 1);
			}
		}
		else if(currentAnimState == AnimState.Run)
		{
			if(currentAnim.IsName("Run"))
			{
				animator.SetInteger("AnimationState", -1);
			}
			else
			{
				animator.SetInteger("AnimationState", 2);
			}
		}
		else if(currentAnimState == AnimState.Jump)
		{
			if(currentAnim.IsName("Jump"))
			{
				animator.SetInteger("AnimationState", -1);
			}
			else
			{
				animator.SetInteger("AnimationState", 3);
			}
		}
		else if(currentAnimState == AnimState.Fall)
		{
			if(currentAnim.IsName("Fall"))
			{
				animator.SetInteger("AnimationState", -1);
			}
			else
			{
				animator.SetInteger("AnimationState", 4);
			}
		}
		else if(currentAnimState == AnimState.Tounge)
		{
			if(currentAnim.IsName("ToungeLaunch") || currentAnim.IsName("ToungeSustain"))
			{
				animator.SetInteger("AnimationState", -1);
			}
			else
			{
				animator.SetInteger("AnimationState", 5);
			}
		}
		else if(currentAnimState == AnimState.Die)
		{
			if(MainMechanics.mechanics.hasDiedTo == MainMechanics.CauseOfDeath.NotDead)
			{
				MainMechanics.mechanics.hasDiedTo = MainMechanics.CauseOfDeath.Villagers;
			}

			if(currentAnim.IsName("Die"))
			{
				animator.SetInteger("AnimationState", -1);
				for(int i = 0; i <bodyParts.Count; i++)
				{
					Color currentColor = bodyParts[i].color;
					bodyParts[i].color = new Color(currentColor.r,currentColor.g,currentColor.b,1-currentAnim.normalizedTime);
				}
			}
			else
			{
				animator.SetInteger("AnimationState", 7);
				MakeSound(death);
			}
		}
	}
	}
}
