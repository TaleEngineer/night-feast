﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {

	public GameObject target;

	// Use this for initialization
	void Awake () 
	{
		if(target != null)
		{
			transform.position = target.transform.position+ new Vector3(0,0.5f,-10);
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(target != null)
		{
			transform.position = target.transform.position+ new Vector3(0,0.5f,-10);
		}
	}
}
