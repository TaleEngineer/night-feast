﻿using UnityEngine;
using System;
using System.Collections;

public class Bullet : MonoBehaviour 
{

	bool hasHit = false;
	public float speed;
	public int damage;
	public float knockback;
	public float timer;



	// Update is called once per frame
	void Update () 
	{
		timer -= Time.deltaTime;
		float distance = speed*Time.deltaTime;
		RaycastHit2D rayHit = Physics2D.Raycast(transform.position, transform.right*transform.localScale.x,distance);
		if(rayHit.collider != null && hasHit == false)
		{
			if(rayHit.collider.GetComponentInParent<Player>())
			{
				Player.player.TakeDamage(damage);
				Player.player.GetComponent<Rigidbody2D>().velocity = transform.right * knockback;
				Destroy (gameObject);
				Instantiate(Player.player.bloodPrefab, transform.position, transform.rotation);
			}
		}
		transform.position += transform.right*speed*Time.deltaTime;
		if(timer <= 0)
		{
			Destroy (gameObject);
		}
	}

	void OnTriggerEnter2D(Collider2D collider)
	{
		if(collider.GetComponentInParent<Player>() && hasHit == false)
		{
			Player hitPerson = collider.GetComponentInParent<Player>();
			hitPerson.TakeDamage(damage);
			hitPerson.GetComponent<Rigidbody2D>().velocity = transform.right * knockback;
			Destroy (gameObject);
			Instantiate(Player.player.bloodPrefab, transform.position, transform.rotation);
		}
	}
	
	void OnTriggerStay2D(Collider2D collider)
	{
		if(collider.GetComponentInParent<Player>() && hasHit == false)
		{
			Player hitPerson = collider.GetComponentInParent<Player>();
			hitPerson.TakeDamage(damage);
			hitPerson.GetComponent<Rigidbody2D>().velocity = transform.right * knockback;
			Destroy (gameObject);
			Instantiate(Player.player.bloodPrefab, transform.position, transform.rotation);
		}
	}
}
