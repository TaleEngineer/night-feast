﻿using UnityEngine;
using System.Collections;

public class ToungeTip : ToungePiece
{
	public static ToungeTip theTip;
	public bool retracting = false;
	bool dialating = true;
	public bool hitSomething = false;
	GameObject pointOfImpact;
	Human hitTarget;

	Vector3 previousPosition = Vector3.zero;

	// Use this for initialization
	void Awake () 
	{
		if(theTip != this && theTip != null)
		{
			DestroyImmediate(theTip.gameObject);
		}
		theTip = this;
	}

	public void clearTarget()
	{
		dialating = true;
		Player.player.readyToPulse = true;
		hitSomething = false;
		retracting = true;
		if(hitTarget != null)
		{
			hitTarget.currentAnimState = Human.AnimState.Idle;
			hitTarget.animator.Play("Idle");
		}
		if(pointOfImpact != null)
		{
			Destroy(pointOfImpact);
		}
	}

	void hasHitTarget(Collider2D collider, Vector2 point)
	{
		if(collider.GetComponentInParent<Human>().health > 0 && !retracting)
		{
			hitTarget = collider.GetComponentInParent<Human>();
			hitTarget.currentAnimState = Human.AnimState.Stunned;
			hitTarget.currentKnowledgeState = Human.KnowledgeState.Knows;
			hitSomething = true;
			pointOfImpact = new GameObject();
			pointOfImpact.transform.position = new Vector3(point.x, point.y, hitTarget.bodyParts[0].transform.position.y);
			pointOfImpact.transform.parent = hitTarget.transform;
			pointOfImpact.name = "Point of Impact";
			transform.position = pointOfImpact.transform.position;

			
			Instantiate(Player.player.bloodPrefab, pointOfImpact.transform.position, Quaternion.Euler(0,0,0));

			hitTarget.MakeSound(MainMechanics.mechanics.toungeStick);

			hitTarget.MakeSound(hitTarget.hitCall);
		}
	}





	void OnTriggerEnter2D(Collider2D collider)
	{
		if(collider.GetComponentInParent<Human>() && hitTarget == null && !retracting)
		{
			hasHitTarget(collider, transform.position);
		}
	}
	
	void OnTriggerStay2D(Collider2D collider)
	{
		if(collider.GetComponentInParent<Human>() && hitTarget == null && !retracting)
		{
			hasHitTarget(collider, transform.position);
		}
	}






	// Update is called once per frame
	void Update () 
	{

		
		if(retracting && previousPosition == transform.position)
		{
			Player.player.DestroyTounge();
		}
		else if(hitSomething)
		{
			if((retracting == false && Vector2.Distance(transform.position, Player.player.transform.position) >= 3) || (hitTarget == null || hitTarget.health <= 0))
			{
				clearTarget();
			}
			else if(!retracting)
			{
				if(pointOfImpact != null)
				{
					transform.position = pointOfImpact.transform.position;
				}
				if(Player.player.readyToPulse)
				{
					if(dialating)
					{
						transform.localScale = new Vector3(Player.player.transform.localScale.x, transform.localScale.y+Time.deltaTime*10,1);
						if(transform.localScale.y > 2)
						{
							transform.localScale = new Vector3(Player.player.transform.localScale.x, 2, 1);
							dialating = false;
							hitTarget.health -= 10;
							Player.player.MakeSound(Player.player.slurp);
							if(hitTarget.health <= 0)
							{
								clearTarget();
							}
						}
					}
					else
					{
						transform.localScale = new Vector3(Player.player.transform.localScale.x, transform.localScale.y-Time.deltaTime*10,1);
						if(transform.localScale.y < 1)
						{
							transform.localScale = new Vector3(Player.player.transform.localScale.x, 1, 1);
							dialating = true;
							Player.player.readyToPulse = false;
						}
					}
				}
			}
			else
			{
				clearTarget();
			}

		}
		else if(retracting == false && Vector2.Distance(transform.position, Player.player.transform.position) < 3)
		{
			float distance = 0f;

			if(Vector2.Distance(transform.position + transform.right*Player.player.transform.localScale.x*15*Time.deltaTime, Player.player.transform.position) > 3)
			{
				distance = 3 - Vector2.Distance(transform.position, Player.player.transform.position);
			}
			else
			{
				distance = 15*Time.deltaTime;
			}

			RaycastHit2D rayHit = Physics2D.Raycast(transform.position, transform.right*Player.player.transform.localScale.x,distance);
			if(rayHit.collider != null)
			{
				if(rayHit.collider.GetComponentInParent<Human>())
				{
					hasHitTarget(rayHit.collider, rayHit.point);
				}
			}

			transform.position += transform.right*15*Time.deltaTime*Player.player.transform.localScale.x;
			if(Vector2.Distance(transform.position, Player.player.transform.position) > 3)
			{
				Vector2 relativePos = transform.position - Player.player.transform.position;
				relativePos.Normalize();
				relativePos *= 3;

				Vector2 newPos = Player.player.transform.position+ new Vector3(relativePos.x, relativePos.y, 0);

				transform.position = new Vector3( newPos.x, newPos.y, 0);
				retracting = true;
				clearTarget();
			}
		}

		if(retracting)
		{
			previousPosition = transform.position;
		}
	}
}
