﻿using UnityEngine;
using System.Collections;

public class PassableFloor : MonoBehaviour 
{
	BoxCollider2D[] boxColliders;

	void Awake()
	{
		boxColliders = GetComponents<BoxCollider2D>();
	}

	void Update () 
	{
		if(Player.player.GetComponent<Rigidbody2D>().velocity.y > 0.1 || Input.GetAxisRaw("Vertical") < 0)
		{
			for(int i = 0; i < boxColliders.Length; i++)
			{
				Physics2D.IgnoreCollision(boxColliders[i], Player.player.GetComponent<BoxCollider2D>());
			}
		}
		else
		{
			for(int i = 0; i < boxColliders.Length; i++)
			{
				Physics2D.IgnoreCollision(boxColliders[i], Player.player.GetComponent<BoxCollider2D>(), false);
			}
		}
	}
}
