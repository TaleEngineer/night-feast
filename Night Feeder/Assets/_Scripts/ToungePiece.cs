﻿using UnityEngine;
using System;
using System.Collections;

public class ToungePiece : MonoBehaviour 
{
	public ToungePiece parent;
	public ToungePiece child;
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	public void UpdatePosition () 
	{
		if(ToungeTip.theTip.retracting)
		{
			if(child != null && this != ToungeTip.theTip)
			{
				if(Mathf.Abs(child.transform.position.x-transform.position.x) > 0.01f)
				{
					if(child.transform.position.x-transform.position.x < 0)
					{
						transform.position = new Vector3(child.transform.position.x + 0.01f,transform.position.y,0);
					}
					else
					{
						transform.position = new Vector3(child.transform.position.x - 0.01f,transform.position.y,0);
					}
				}
			}
			else if(child != null && this == ToungeTip.theTip)
			{
				if(Mathf.Abs(child.transform.position.x-transform.position.x) > 0.078f)
				{
					if(child.transform.position.x-transform.position.x < 0)
					{
						transform.position = new Vector3(child.transform.position.x + 0.078f,transform.position.y,0);
					}
					else
					{
						transform.position = new Vector3(child.transform.position.x - 0.078f,transform.position.y,0);
					}
				}
			}
			else if(child == null)
			{
				Vector3 dirToGo3D = Player.player.spewPoint.transform.position - transform.position;
				Vector2 dirToGo = new Vector2(dirToGo3D.x, dirToGo3D.y);
				dirToGo.Normalize();
				dirToGo *= Time.deltaTime*15;

				transform.position += new Vector3(dirToGo.x, dirToGo.y, 0);
			}
			
			if(parent != null)
			{
				parent.UpdatePosition();
			}
		}
		else
		{
			if(parent != null && parent != ToungeTip.theTip)
			{
				if(Mathf.Abs(parent.transform.position.x-transform.position.x) > 0.01f)
				{
					if(Player.player.transform.localScale.x < 0)
					{
						transform.position = new Vector3(parent.transform.position.x + 0.01f,transform.position.y,0);
					}
					else
					{
						transform.position = new Vector3(parent.transform.position.x - 0.01f,transform.position.y,0);
					}
				}
			}
			else if(parent != null && parent == ToungeTip.theTip)
			{
				if(Mathf.Abs(parent.transform.position.x-transform.position.x) > 0.078f)
				{
					if(Player.player.transform.localScale.x < 0)
					{
						transform.position = new Vector3(parent.transform.position.x + 0.078f,transform.position.y,0);
					}
					else
					{
						transform.position = new Vector3(parent.transform.position.x - 0.078f,transform.position.y,0);
					}
				}
			}

			if(parent != null && Mathf.Abs(parent.transform.position.y-transform.position.y) > 0.01f)
			{
				if(parent.transform.position.y-transform.position.y < 0)
				{
					transform.position = new Vector3(transform.position.x,parent.transform.position.y + 0.01f,0);
				}
				else
				{
					transform.position = new Vector3(transform.position.x,parent.transform.position.y - 0.01f,0);
				}
			}


			if(child != null)
			{
				child.UpdatePosition();
			}
		}
	}

	public void UpdateSize()
	{

		if(parent != null && transform.localScale.y != parent.transform.localScale.y)
		{
			transform.localScale = new Vector3(1, parent.transform.localScale.y, 1);
		}

		if(ToungeTip.theTip.retracting)
		{
			
		}
		if(child != null && Mathf.Abs(child.transform.position.y-transform.position.y) > 0.01f)
		{
			if(child.transform.position.y-transform.position.y < 0)
			{
				transform.position = new Vector3(transform.position.x,child.transform.position.y + 0.01f,0);
			}
			else
			{
				transform.position = new Vector3(transform.position.x,child.transform.position.y - 0.01f,0);
			}
		}
		else if(child == null)
		{
			transform.position = new Vector3(transform.position.x, Player.player.spewPoint.transform.position.y, 0);
		}

		if(parent != null)
		{
			parent.UpdateSize();
		}
	}
}
