﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class Human : MonoBehaviour 
{
	public enum AnimState
	{
		Idle, //0
		Walk, //1
		Run, //2
		Jump, //3
		Fall, //4
		Stunned, //6
		Attacking,
		ClimbingUp, //10
		ClimbingDown //11
	}

	public enum KnowledgeState
	{
		Oblivious,
		Curious,
		Suspicious,
		Knows,
		ADMIN_FOLLOW
	}

	public float timeSinceLastSpotting = 0;
	public float turnTimer = 0;
	public float meanderTimer = 0;

	public List<Weapon> arsenal = new List<Weapon>();

	public Pathfinder pathFinder = new Pathfinder();

	public PathNode closestNode;
	public List<PathNode> pathToGo = new List<PathNode>();
	public PathNode lastSpotedMonster;
	public PathNode previousNode;
	public List<PathNode> OverrideList = new List<PathNode>();

	public KnowledgeState currentKnowledgeState;

	public List<SpriteRenderer> bodyParts = new List<SpriteRenderer>();

	public AnimState currentAnimState = AnimState.Idle;
	public Animator animator;
	
	public bool hasJumped = false;
	public bool sprinting = false;

	public int health = 100;

	float visibility = 1f;

	public Rigidbody2D rigidBody;

	bool deathRecoded = false;

	public List<Sprite> hairOptions = new List<Sprite>();
	public List<Sprite> eyeOptions = new List<Sprite>();

	public List<SpriteRenderer> eyes = new List<SpriteRenderer>();
	public List<SpriteRenderer> hair = new List<SpriteRenderer>();

	public AudioSource audioSource;
	public AudioClip hitCall;
	public AudioClip deathCall;

	public void MakeSound(AudioClip sound)
	{
		float pitch = UnityEngine.Random.Range(0.875f, 1.2f);
		audioSource.pitch = pitch;
		audioSource.PlayOneShot(sound);
	}

	void Start () 
	{
		Physics2D.IgnoreCollision(GetComponent<BoxCollider2D>(), Player.player.GetComponent<BoxCollider2D>());
		animator = GetComponent<Animator>();
		rigidBody = GetComponent<Rigidbody2D>();

		//bodyParts[0].transform.position += new Vector3(0, UnityEngine.Random.Range (-0.25f,0.128f),0);

		transform.position += new Vector3(0,0,bodyParts[0].transform.position.y);

		if(hairOptions.Count > 1)
		{
			int choice = UnityEngine.Random.Range (0, hairOptions.Count);

			for(int i = 0; i < hair.Count; i++)
			{
				hair[i].sprite = hairOptions[choice];
			}
		}

		if(eyeOptions.Count > 1)
		{
			int choice = UnityEngine.Random.Range (0, eyeOptions.Count);
			
			for(int i = 0; i < eyes.Count; i++)
			{
				eyes[i].sprite = eyeOptions[choice];
			}
		}
	}

	public bool CanSeePlayer()
	{
		int viewSight = 0;
		
		if(currentKnowledgeState == KnowledgeState.Knows || Player.player.currentAnimState == Player.AnimState.Tounge|| Player.player.currentAnimState == Player.AnimState.ToungeRetract)
		{
			viewSight = 10;
		}
		else if(currentKnowledgeState == KnowledgeState.Oblivious)
		{
			viewSight = 3;
		}
		else if(currentKnowledgeState == KnowledgeState.Curious)
		{
			viewSight = 6;
		}
		else if(currentKnowledgeState == KnowledgeState.Suspicious)
		{
			viewSight = 8;
		}
		else if(currentKnowledgeState == KnowledgeState.ADMIN_FOLLOW)
		{
			return true;
		}

		float distance = Vector2.Distance (Player.player.transform.position, transform.position);
		if(distance <= viewSight)
		{
			Vector2 playerDir = Player.player.transform.position - transform.position;
			Vector2 forward = transform.right;
			float angel = Vector2.Angle(playerDir, forward);

			if(angel < 45)
			{

				RaycastHit2D[] rayHit = Physics2D.RaycastAll(transform.position, playerDir,distance);
				if(rayHit.Length > 0)
				{
					for(int i = 0; i < rayHit.Length; i++)
					{
						if(rayHit[i].collider.CompareTag("SightBlocker"))
						{
							return false;
						}
					}
				}
				return true;
			}
			
		}

		if(currentKnowledgeState == KnowledgeState.Knows && Vector2.Distance(Player.player.transform.position, transform.position) < 2)
		{
			faceMonster();
			if(Vector2.Distance(Player.player.transform.position, transform.position) <= viewSight)
			{
				Vector2 playerDir = Player.player.transform.position - transform.position;
				Vector2 forward = transform.right;
				float angel = Vector2.Angle(playerDir, forward);
				
				if(angel < 45)
				{
					return true;
				}
				
			}
		}
		return false;
	}

	public void MoveInDirection(bool GoingLeft)
	{
		if(GoingLeft)
		{
			transform.rotation = Quaternion.Euler(0, 180, 0);
		}
		else
		{
			transform.rotation = Quaternion.Euler(0, 0, 0);
		}
		
		if(sprinting)
		{
			rigidBody.velocity = transform.right*6;
			transform.position += transform.right*0.001f;
			if(currentAnimState != AnimState.Jump && currentAnimState != AnimState.Fall)
			{
				currentAnimState = AnimState.Run;
			}
		}
		else
		{
			rigidBody.velocity =  transform.right*3;
			transform.position += transform.right*0.001f;
			if(currentAnimState != AnimState.Jump && currentAnimState != AnimState.Fall)
			{
				currentAnimState = AnimState.Walk;
			}
		}
	}

	public void ClimbInDirection(bool GoingUp)
	{
		rigidBody.gravityScale = 0;
		rigidBody.velocity = Vector2.zero;
		transform.rotation = Quaternion.Euler(0, 0, 0);
		GetComponent<BoxCollider2D>().isTrigger = true;

		if(GoingUp)
		{
			currentAnimState = AnimState.ClimbingUp;
			transform.position += new Vector3(0, 2*Time.deltaTime,0);
		}
		else
		{
			currentAnimState = AnimState.ClimbingDown;
			transform.position -= new Vector3(0, 2*Time.deltaTime,0);
		}
	}

	public void faceMonster()
	{
		if(Player.player.transform.position.x < transform.position.x)
		{
			transform.rotation = Quaternion.Euler (0,180,0);
		}
		else
		{
			transform.rotation = Quaternion.Euler (0,0,0);
		}
	}

	public void turnAround()
	{
		transform.rotation = Quaternion.Euler (0,transform.rotation.eulerAngles.y + 180,0);
		turnTimer = 5;
	}

	public virtual void Meander()
	{
		int numberOfJumps = UnityEngine.Random.Range (3, 11);
		PathNode currentNode = closestNode;
		for(int i = 0; i < numberOfJumps; i++)
		{
			int pathToChoose = UnityEngine.Random.Range (0, currentNode.Neighbors.Count);
			bool newPath = false;
			if(pathToGo != null && !pathToGo.Contains(currentNode.Neighbors[pathToChoose]))
			{
				pathToGo.Add(currentNode.Neighbors[pathToChoose]);
				currentNode = currentNode.Neighbors[pathToChoose];
			}
		}
		meanderTimer = 5;
	}

	public void goPath()
	{
		if(pathToGo[0].nodeType == PathNode.NodeType.WalkNode || previousNode.nodeType == PathNode.NodeType.WalkNode)
		{
			if(pathToGo[0].transform.position.x < transform.position.x)
			{
				MoveInDirection(true);
			}
			else if(pathToGo[0].transform.position.x > transform.position.x)
			{
				MoveInDirection(false);
			}
			if(Mathf.Abs(pathToGo[0].transform.position.x - transform.position.x) < 0.1f)
			{
				GetComponent<BoxCollider2D>().isTrigger = false;
				rigidBody.gravityScale = 1;
				previousNode = pathToGo[0];
				pathToGo.RemoveAt(0);
				
				if(OverrideList.Count > 0)
				{
					pathToGo = OverrideList;
					OverrideList.Clear();
				}
			}
		}
		else if(pathToGo[0].nodeType == PathNode.NodeType.ClimbNode)
		{
			if(pathToGo[0].transform.position.y < transform.position.y)
			{
				ClimbInDirection(false);
			}
			else if(pathToGo[0].transform.position.y > transform.position.y)
			{
				ClimbInDirection(true);
			}

			if(Mathf.Abs(pathToGo[0].transform.position.y - transform.position.y) < 0.1f)
			{
				GetComponent<BoxCollider2D>().isTrigger = false;
				rigidBody.gravityScale = 1;
				previousNode = pathToGo[0];
				pathToGo.RemoveAt(0);

				if(OverrideList.Count > 0)
				{
					pathToGo = OverrideList;
					OverrideList.Clear();
				}
			}
		}
	}
	
	public virtual void AI()
	{
		bool playerSpotted = CanSeePlayer();
		if(playerSpotted)
		{
			turnTimer = 0;
		}

		if(playerSpotted && (Player.player.currentAnimState == Player.AnimState.Tounge || Player.player.currentAnimState == Player.AnimState.ToungeRetract))
		{
			currentKnowledgeState = KnowledgeState.Knows;
		}

		if(currentKnowledgeState == KnowledgeState.Oblivious)
		{
			if(pathToGo.Count == 0 && meanderTimer <= 0)
			{
				Meander();
			}
			else if(pathToGo.Count > 0)
			{
				goPath ();
			}
			else
			{
				currentAnimState = AnimState.Idle;
			}

			if(playerSpotted && Player.player.currentAnimState == Player.AnimState.Run)
			{
				pathToGo.Clear();
				currentKnowledgeState = KnowledgeState.Curious;
				currentAnimState = AnimState.Idle;
			}
			if(playerSpotted && Player.player.currentAnimState == Player.AnimState.Jump)
			{
				pathToGo.Clear();
				currentKnowledgeState = KnowledgeState.Suspicious;
				currentAnimState = AnimState.Idle;
			}
		}//OBLIVIOUS
		else if(currentKnowledgeState == KnowledgeState.Curious)
		{
			if(playerSpotted == false)
			{
				turnAround ();
				if(CanSeePlayer() == false)
				{
					currentKnowledgeState = KnowledgeState.Oblivious;
				}
			}
		}//CURIOUS
		else if(currentKnowledgeState == KnowledgeState.Suspicious)
		{
			if(playerSpotted == false && turnTimer <= 0)
			{
				turnAround();
			}


			if(playerSpotted && pathToGo.Count == 0)
			{
				pathToGo = pathFinder.pathfind(closestNode, Player.player.closestNode);
			}
			if(pathToGo.Count > 0)
			{
				goPath ();
			}
			else
			{
				currentAnimState = AnimState.Idle;
			}
		}//SUSPICIOUS
		else if (currentKnowledgeState == KnowledgeState.Knows)
		{
			sprinting = true;
			if( turnTimer <= 0 && playerSpotted == false)
			{
				turnAround();
			}
			currentAnimState = AnimState.Idle;

		}//KNOWS
		else if(currentKnowledgeState == KnowledgeState.ADMIN_FOLLOW)
		{
			if(pathToGo.Count == 0)
			{
				pathToGo = pathFinder.pathfind(closestNode, Player.player.closestNode);
			}
			else if(pathToGo.Count > 0)
			{
				goPath();
			}
		}


		/*if(currentAnimState != AnimState.Fall)
		{
			currentAnimState = AnimState.Jump;
		}
		else if(Input.GetAxisRaw("Horizontal") != 0)
		{
			float dir = Input.GetAxisRaw("Horizontal");
			int negative = -1;
			if(dir < 0)
			{
				negative = 1;
				transform.localScale = new Vector3(-1,1,1);
			}
			else
			{
				transform.localScale = new Vector3(1,1,1);
			}
			
			if(Input.GetButton("Sprint"))
			{
				rigidBody.velocity = new Vector2(dir*6, rigidBody.velocity.y);
				transform.rotation = Quaternion.Euler(new Vector3(0,0,10*negative));
				if(currentAnimState != AnimState.Jump && currentAnimState != AnimState.Fall)
				{
					currentAnimState = AnimState.Run;
				}
			}
			else
			{
				rigidBody.velocity = new Vector2(dir*3, rigidBody.velocity.y);
				transform.rotation = Quaternion.Euler(new Vector3(0,0,5*negative));
				if(currentAnimState != AnimState.Jump && currentAnimState != AnimState.Fall)
				{
					currentAnimState = AnimState.Walk;
				}
			}
			
			
		}
		else
		{
			if(currentAnimState != AnimState.Jump && currentAnimState != AnimState.Fall)
			{
				currentAnimState = AnimState.Idle;
			}
			transform.rotation = Quaternion.Euler(new Vector3(0,0, 0));
		}*/
	}

	// Update is called once per frame
	void Update () 
	{
		AnimatorStateInfo currentAnim = animator.GetCurrentAnimatorStateInfo(0);
		Rigidbody2D rigidBody = GetComponent<Rigidbody2D>();

		if(Player.player.currentHealth <= 0)
		{
			currentKnowledgeState = KnowledgeState.Curious;
			currentAnimState = AnimState.Idle;
		}
		else
		{
			if(turnTimer > 0)
			{
				turnTimer -= Time.deltaTime;
			}
			if(meanderTimer > 0)
			{
				meanderTimer -= Time.deltaTime;
			}
			if(previousNode == null)
			{
				previousNode = closestNode;
			}

			if(currentKnowledgeState == KnowledgeState.Knows)
			{
				if(CanSeePlayer())
				{
					timeSinceLastSpotting = 0;
				}
				else
				{
					timeSinceLastSpotting += Time.deltaTime;
				}
			}
			
			if(currentAnimState != AnimState.Stunned && health > 0)
			{
				/*if(pathToGo.Count == 0 && closestNode.nodeType == PathNode.NodeType.ClimbNode && Mathf.Abs(closestNode.transform.position.y - transform.position.y) > 0.1f &&
				   currentAnimState != AnimState.Jump && currentAnimState != AnimState.ClimbingDown && currentAnimState != AnimState.ClimbingUp)
				{
					pathToGo.Add(closestNode);
				}*/
				if(currentAnimState == AnimState.Jump && hasJumped == false)
				{
					if(currentAnim.IsName("Jump") && currentAnim.normalizedTime >= 0.08695f)
					{
						rigidBody.velocity = new Vector2(rigidBody.velocity.x, 4);
						hasJumped = true;
					}
				}
				else if(currentAnimState != AnimState.Attacking)
				{
					if(rigidBody.velocity.y < -1f)
					{
						currentAnimState = AnimState.Fall;
					}
					if(currentAnimState == AnimState.Fall)
					{
						if(rigidBody.velocity.y == 0)
						{
							hasJumped = false;
							currentAnimState = AnimState.Idle;
						}
					}


				}

				if((currentAnimState == AnimState.ClimbingUp || currentAnimState == AnimState.ClimbingDown) && pathToGo.Count > 0)
				{
					goPath();
				}
				else
				{
					AI ();
				}
			}
		}
		if(currentAnimState == AnimState.Attacking)
		{
			if(currentAnim.IsTag("Attack"))
			{
				animator.SetInteger("AnimationState", -1);
				if(currentAnim.normalizedTime >= 1)
				{
					currentAnimState = AnimState.Idle;
					animator.Play("Idle");
				}
			}
			else
			{
				currentAnimState = AnimState.Idle;
			}
		}
		else if(currentAnimState == AnimState.Idle)
		{
			if(currentAnim.IsName("Idle"))
			{
				animator.SetInteger("AnimationState", -1);
			}
			else
			{
				animator.SetInteger("AnimationState", 0);;
			}
		}
		else if(currentAnimState == AnimState.Walk)
		{
			if(currentAnim.IsName("Walk"))
			{
				animator.SetInteger("AnimationState", -1);
			}
			else
			{
				animator.SetInteger("AnimationState", 1);;
			}
		}
		else if(currentAnimState == AnimState.Run)
		{
			if(currentAnim.IsName("Run"))
			{
				animator.SetInteger("AnimationState", -1);
			}
			else
			{
				animator.SetInteger("AnimationState", 2);;
			}
		}
		else if(currentAnimState == AnimState.Jump)
		{
			if(currentAnim.IsName("Jump"))
			{
				animator.SetInteger("AnimationState", -1);
			}
			else
			{
				animator.SetInteger("AnimationState", 3);;
			}
		}
		else if(currentAnimState == AnimState.Fall)
		{
			if(currentAnim.IsName("Fall"))
			{
				animator.SetInteger("AnimationState", -1);
			}
			else
			{
				animator.SetInteger("AnimationState", 4);;
			}
		}
		else if(currentAnimState == AnimState.Stunned)
		{
			if(currentAnim.IsName("BloodStunned"))
			{
				animator.SetInteger("AnimationState", -1);
			}
			else
			{
				animator.SetInteger("AnimationState", 6);
			}
		}

		if(health <= 0)
		{
			if(deathRecoded == false)
			{
				MainMechanics.mechanics.kills++;
				deathRecoded = true;
				
				MakeSound(deathCall);
			}
			if(currentAnim.IsName("Die") || currentAnim.IsName("DieStagnation"))
			{
				animator.SetInteger("AnimationState", -1);
			}
			else
			{
				animator.SetInteger("AnimationState", 5);
			}

			visibility -= Time.deltaTime;

			for(int i = 0; i < bodyParts.Count; i++)
			{
				Color currentColor = bodyParts[i].color;
				bodyParts[i].color = new Color(currentColor.r,currentColor.g,currentColor.b, visibility);
			}

			if(visibility <= 0)
			{
				Destroy(gameObject);
			}
		}
	}
}
