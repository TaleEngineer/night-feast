﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DapperMan : Human 
{
	public override void AI ()
	{
		bool playerSpotted = CanSeePlayer();		
		if(currentKnowledgeState == KnowledgeState.Oblivious)
		{
			base.AI ();
		}//OBLIVIOUS
		else if(currentKnowledgeState == KnowledgeState.Curious)
		{
			base.AI ();
		}//CURIOUS
		else if(currentKnowledgeState == KnowledgeState.Suspicious)
		{

			base.AI ();
		}//SUSPICIOUS
		else if (currentKnowledgeState == KnowledgeState.Knows)
		{
			if(playerSpotted)
			{
				lastSpotedMonster = Player.player.closestNode;
				pathToGo.Clear();
			}
			sprinting = true;
			if(playerSpotted && currentAnimState != AnimState.Attacking)
			{
				if(Mathf.Abs(Player.player.transform.position.x - transform.position.x) <= 1.2f)
				{
					currentAnimState = AnimState.Attacking;
					arsenal[0].SetAttack(this, 50, 10, "Stab", 0.675f);
					animator.SetInteger("AnimationState", 7);
				}
				else if(Mathf.Abs(Player.player.transform.position.y - transform.position.y) <= 0.5f)
				{
					currentAnimState = AnimState.Attacking;
					RangerWeapon gun = arsenal[1] as RangerWeapon;
					gun.SetAttack(this, 0, 0, "Shoot", 0.617f, 1, false, false, 0.8f);
					animator.SetInteger("AnimationState", 8);
				}
			}
			else if(playerSpotted == false)
			{
				if(pathToGo.Count > 0)
				{
					goPath();
				}
				else if(lastSpotedMonster != closestNode && timeSinceLastSpotting < 50f)
				{
					pathToGo = pathFinder.pathfind(closestNode, lastSpotedMonster);
					if(pathToGo.Count == 0)
					{
						pathToGo.Add (lastSpotedMonster);
					}
				} 
				else if(meanderTimer <= 0)
				{
					Meander();
				}
				else
				{
					base.AI();
				}
			}
			
		}//KNOWS
		else
		{
			base.AI ();
		}
			
			
			/*if(currentAnimState != AnimState.Fall)
		{
			currentAnimState = AnimState.Jump;
		}
		else if(Input.GetAxisRaw("Horizontal") != 0)
		{
			float dir = Input.GetAxisRaw("Horizontal");
			int negative = -1;
			if(dir < 0)
			{
				negative = 1;
				transform.localScale = new Vector3(-1,1,1);
			}
			else
			{
				transform.localScale = new Vector3(1,1,1);
			}
			
			if(Input.GetButton("Sprint"))
			{
				rigidBody.velocity = new Vector2(dir*6, rigidBody.velocity.y);
				transform.rotation = Quaternion.Euler(new Vector3(0,0,10*negative));
				if(currentAnimState != AnimState.Jump && currentAnimState != AnimState.Fall)
				{
					currentAnimState = AnimState.Run;
				}
			}
			else
			{
				rigidBody.velocity = new Vector2(dir*3, rigidBody.velocity.y);
				transform.rotation = Quaternion.Euler(new Vector3(0,0,5*negative));
				if(currentAnimState != AnimState.Jump && currentAnimState != AnimState.Fall)
				{
					currentAnimState = AnimState.Walk;
				}
			}
			
			
		}
		else
		{
			if(currentAnimState != AnimState.Jump && currentAnimState != AnimState.Fall)
			{
				currentAnimState = AnimState.Idle;
			}
			transform.rotation = Quaternion.Euler(new Vector3(0,0, 0));
		}*/
	}
}
