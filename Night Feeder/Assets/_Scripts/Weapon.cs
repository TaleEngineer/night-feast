﻿using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour 
{
	public Human user;
	public int damage;
	public float knockback;
	public bool hasHit;
	public string animationName;
	public float hitFrame;

	public AudioSource audioSource;

	void Awake()
	{
		audioSource = GetComponent<AudioSource>();
	}

	public void MakeSound(AudioClip sound)
	{
		audioSource.pitch = Random.Range(0.875f, 1.2f);
		audioSource.PlayOneShot(sound);
	}

	public void SetAttack(Human _user, int _damage, float _knockback, string _animName, float attackFrame)
	{
		user = _user;
		damage = _damage;
		knockback = _knockback;
		animationName = _animName;
		hasHit = false;
		hitFrame = attackFrame;
	}

	void Update()
	{
		if(user != null && user.GetComponent<Animator>())
		{
			AnimatorStateInfo info = user.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0);
			if(info.IsName(animationName) && info.normalizedTime >= 1)
			{
				hasHit = true;
			}
		}

	}

	void OnTriggerEnter2D(Collider2D collider)
	{
		if(collider.GetComponentInParent<Player>() && hasHit == false && user.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime >= hitFrame)
		{
			Player hitPerson = collider.GetComponentInParent<Player>();
			hitPerson.TakeDamage(damage);
			hitPerson.GetComponent<Rigidbody2D>().velocity = user.transform.right*knockback;
			hasHit = true;
		}
	}

	void OnTriggerStay2D(Collider2D collider)
	{
		if(collider.GetComponentInParent<Player>() && hasHit == false && user.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime >= hitFrame)
		{
			Player hitPerson = collider.GetComponentInParent<Player>();
			hitPerson.TakeDamage(damage);
			hitPerson.GetComponent<Rigidbody2D>().velocity = user.transform.right*knockback;
			hasHit = true;
		}
	}
}
