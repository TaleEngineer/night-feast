﻿using UnityEngine;
using System.Collections;

public class HomeBase : MonoBehaviour 
{
	public Camera mainCamera;
	BoxCollider2D entranceArea;
	// Use this for initialization
	void Start () 
	{
		entranceArea = GetComponent<BoxCollider2D>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(entranceArea.OverlapPoint(Player.player.transform.position) && (MainMechanics.mechanics.timeRemaining <= 30 || MainMechanics.mechanics.villagerList.Count == 0))
		{
			MainMechanics.mechanics.timeRemaining += Time.deltaTime;
			MainMechanics.mechanics.HasWon();
		}
	}

	void OnGUI()
	{
		if((entranceArea.OverlapPoint(Player.player.transform.position) || MainMechanics.mechanics.timeRemaining <= 30 || MainMechanics.mechanics.villagerList.Count == 0) 
		   && MainMechanics.mechanics.hasDiedTo == MainMechanics.CauseOfDeath.NotDead && MainMechanics.mechanics.hasWon == false)
		{
			string text = "ERROR!";
			if(MainMechanics.mechanics.timeRemaining <= 30)
			{
				text = "\"Quickly, Master! Before THE BURNING HATE spots you!\"";
			}
			else if(MainMechanics.mechanics.villagerList.Count == 0)
			{
				text = "There are none left to feed on. It is best to return home.";
			}
			else
			{
				text = "It is best to return just before THE BURNING HATE rises.";
			}
			var labelStyle = GUI.skin.GetStyle ("Label");
			labelStyle.fontSize = Screen.height/20;
			labelStyle.alignment = TextAnchor.MiddleCenter;

			//gets the coords relative to the screen not the world.
			Vector3 screenCoords =  mainCamera.WorldToScreenPoint(transform.position);
			
			//Fixes the co-ordinates of the text on screen, has the y co-ordinates drift downward over time
			Vector2 placement = new Vector3(screenCoords.x, Screen.height - screenCoords.y-10);
			
			GUIContent nameCalc = new GUIContent(text);
			
			//determine how big our text area will be
			Vector2 nameSize = labelStyle.CalcSize(nameCalc);
			
			Rect textRectangle = new Rect(placement.x-(nameSize.x/2),placement.y-(nameSize.y/2), nameSize.x, nameSize.y); 
			
			GUI.Label(textRectangle, text);
		}
	}
}
