﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PathNode : MonoBehaviour 
{
	public List<PathNode> Neighbors = new List<PathNode>();

	public enum NodeType
	{
		WalkNode,
		JumpNode,
		ClimbNode
	}

	public NodeType nodeType;

	void OnTriggerStay2D(Collider2D collider)
	{
		if(collider.GetComponent<Player>())
		{
			collider.GetComponent<Player>().closestNode = this;
		}
		else if(collider.GetComponent<Human>())
		{
			collider.GetComponent<Human>().closestNode = this;
		}
	}

	void OnTriggerEnter2D(Collider2D collider)
	{
		if(collider.GetComponent<Player>())
		{
			collider.GetComponent<Player>().closestNode = this;
		}
		else if(collider.GetComponent<Human>())
		{
			collider.GetComponent<Human>().closestNode = this;
		}
	}
}
