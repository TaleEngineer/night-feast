﻿using UnityEngine;
using System.Collections;

public class BloodExplosion : MonoBehaviour {

	// Use this for initialization
	void Start () 
	{

	}
	
	// Update is called once per frame
	void Update () 
	{
		if(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime >=1)
		{
			Destroy (gameObject);
		}
	}
}
